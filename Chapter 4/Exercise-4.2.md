# Exercise 4.2 

Let **R** be a database schema and *q* a rule.
(a) Prove that q(**I**) is finite for each instance **I** over R.
(b) Show an upper bound, given instance I of R and output arity for conjunctive query q,
for the number of tuples that can occur in q(I). Show that this bound can be achieved.

* * *

### (Etienne's Version)

(a) $`q`$ is a rule so is range restricted. By definition, if $`\mathcal{I}`$ is an instance over $`\mathcal{R}`$, 

$`q(\mathcal{I}) = \{ v(u_i) | v \text{ is a valuation over } var(q) \text{ and } v(u_i) \in \mathcal(I)(R_i), i \in [1;n] \}`$

Valuations map terms (tuples with elements in  $`dom \cup var`$) to tuples only with elements from $`dom`$. Then, all constantes in $`v(u_i)`$ are either in $`adom(q)`$, or in $`adom(\mathcal{I})`$. 
So, 

$`q(\mathcal{I}) = adom(q(\mathcal{I})) \subseteq adom(q) \cup adom(\mathcal{I}) = adom(q,\mathcal{I})`$

$`\mathcal{I}`$ is an instance so $`admon(\mathcal{I})`$ is finite, and $`q`$ is a rule so $`admon(q)`$ is also finite. Finally, $`q(\mathcal{I})`$ is finite.

(b) Let $`\mathcal{I}`$ is an instance over $`\mathcal{R}`$, let $`n`$ be the number of tuples in $`\mathcal{I}`$, let $`m`$ be the arity of $`q`$. Then the upper bound of the number of tuples that can occur in $`q(\mathcal{I})`$ is $`n^m`$. This upper bound is achieved when we query all variables separately in every relation instance. For example, if the instance is a single relation called M with arity 3, the query: $`ans(x, y, z) \leftarrow M(x, y_1, z_1), M(x_2, y, z_2), M(x_3, y_3, z)`$ achieve the upper bound $`n^3`$.


* * *

### (Rony's version)

a) 

q : $`ans(u_1,...,u_n) \leftarrow R_1(\vec{v_1})...R_n(\vec{v_n})`$

Let's suppose that it exists an instance **I** over R such that **q(I)** is not finite.

In that case, given that the range restriction is respected, each $`u_i`$ should appear in one of the relation $`R_i`$. 

Then, since $`ans()`$ is infinite, there is at least one relation $`R_k`$ which is infinite and this is
impossible.

So, q(**I**) is finite for each instance **I** over R.

b)

q : $`ans(u_1,...,u_n) \leftarrow R_1(\vec{v_1})...R_n(\vec{v_n})`$

**n** is the arity of the output **q(I)**

Let's write 

$`q(\mathbf{I})=\left\{v(u) \mid v\right.`$ is a valuation over $`\operatorname{var}(q)`$ and $`v(u_{i}) \in \mathbf{I}\left(R_{i}\right)`$ for each $`i \in[1, n]\}`$

Then we deduce that for each $`i \in[1, n]`$, $`|v(u_i)| \leq |\text{I}|`$ 
So, $`|v(u_1) \ v(u_2) \ .... \ v(u_n)| \leq |I|^n`$

*Solution of Etienne for the following :*

This upper bound is achieved when we query all variables separately in every relation instance. For example, if the instance is a single relation called M with arity 3, the query:

$`ans(x, y, z) \leftarrow M(x, y_1, z_1), M(x_2, y, z_2), M(x_3, y_3, z)`$ achieve the upper bound $`|I|^n`$.




